package com.zxd.itoken.service.admin.mapper;

import com.zxd.itoken.service.admin.domain.TbSysUser;
import tk.mybatis.mapper.MyMapper;

public interface TbSysUserMapper extends MyMapper<TbSysUser> {
}