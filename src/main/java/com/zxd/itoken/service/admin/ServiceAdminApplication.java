package com.zxd.itoken.service.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.xml.ws.Service;

/**
 * @program: itoken
 * @description:
 * @author: Mr.Zhao
 * @create: 2019-07-18 08:39
 **/
@EnableEurekaClient
@SpringBootApplication
public class ServiceAdminApplication {

    public static void main(String[] args) {

        SpringApplication.run(ServiceAdminApplication.class,args);
    }
}
